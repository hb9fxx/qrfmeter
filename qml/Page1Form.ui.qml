import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.0

Item {
    property alias devicename: devicename
    property alias devicelist: devicelist
    property alias attenuator: attenuator
    property alias lineargauge: lineargauge
    property alias circulargauge: circulargauge
    property string textgauge2

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        RowLayout {
            id: versionrow
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Text {
                id: devicename
                fontSizeMode: Text.Fit
                font.pixelSize: 32
                color: "#aaffffff"
            }
        }

        RowLayout {
            id: buttonrow
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Text {
                id: devicelabel
                text: qsTr("Device")
                font.pixelSize: 24
                color: "#aaffffff"
            }

            ComboBox {
                id: devicelist
            }

            Item {
                Layout.fillWidth: true
            }

            Text {
                id: attenuationlabel
                text: qsTr("Attenuation")
                font.pixelSize: 24
                color: "#aaffffff"
            }

            ComboBox {
                id: attenuator
            }
        }

        RowLayout {
            id: gaugerow
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Gauge {
                id: lineargauge
                Layout.fillHeight: true
                anchors.margins: 10
            }

            CircularGauge {
                id: circulargauge
                Layout.fillHeight: true
                Layout.fillWidth: true

                style: CircularGaugeStyle {
                    minimumValueAngle: -80
                    maximumValueAngle: 80

                    tickmark: Rectangle {
                        visible: styleData.value < -10 || styleData.value % 10 == 0
                        implicitWidth: outerRadius * 0.02
                        antialiasing: true
                        implicitHeight: outerRadius * 0.06
                        color: styleData.value < rfmeter.rangeLow ? "#aa0000ff" : (styleData.value >= rfmeter.rangeHigh ? "#aaff0000" : "#aa333333")
                    }

                    tickmarkLabel:  Text {
                        font.pixelSize: Math.max(6, outerRadius * 0.1)
                        text: styleData.value
                        color: styleData.value < rfmeter.rangeLow ? "#aa0000ff" : (styleData.value >= rfmeter.rangeHigh ? "#aaff0000" : "#aa333333")
                        antialiasing: true
                    }

                    background:  Item {
                        Rectangle {
                            implicitWidth: outerRadius * 2
                            implicitHeight: outerRadius * 2
                            radius: width / 2
                            color: "#ffddddbb"
                            anchors.centerIn: parent
                        }

                        Text {
                            id: dbtext
                            font.weight: Font.Normal
                            font.family: "Tahoma"
                            fontSizeMode: Text.FixedSize
                            font.pixelSize: outerRadius/2
                            color: "#aa333333"
                            horizontalAlignment: Text.AlignHCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: parent.verticalCenter
                            anchors.topMargin: 0
                            text: textgauge2
                        }

                        Text {
                            font.weight: Font.Normal
                            font.family: "Tahoma"
                            fontSizeMode: Text.FixedSize
                            font.pixelSize: outerRadius/6
                            color: "#aa333333"
                            horizontalAlignment: Text.AlignHCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.top: dbtext.bottom
                            anchors.topMargin: 0
                            text: "dBm"
                        }
                    }
                }
            }
        }
    }
}
