import QtQuick 2.7

import rfmeter 0.1

Page1Form {
    anchors.fill: parent

    RFMeter {
        id: rfmeter
    }

    devicename  {
        text: rfmeter.deviceName
    }

    devicelist  {
        model: rfmeter.deviceList
        onCurrentTextChanged: rfmeter.setDevice(devicelist.currentText)
        onPressedChanged: rfmeter.listSerialPorts()
    }

    attenuator {
        model: [0, -1, -2, -3, -6, -10, -20, -30, -40, -50, -60]
        onCurrentTextChanged: rfmeter.setAttenuator(attenuator.currentText)
    }

    lineargauge {
        minimumValue: rfmeter.rangeMin
        maximumValue: rfmeter.rangeMax
        value: rfmeter.value
    }

    circulargauge {
        minimumValue: rfmeter.rangeMin
        maximumValue: rfmeter.rangeMax
        value: rfmeter.value
    }

    textgauge2 : rfmeter.value.toFixed(1)
}
