#include <QGuiApplication>
#include <QtQml>
#include <QQmlApplicationEngine>

#include "rfmeter.h"

int main(int argc, char *argv[]) {
    // QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling););
    QGuiApplication app(argc, argv);

    qmlRegisterType<RFMeter>("rfmeter", 0, 1, "RFMeter");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
