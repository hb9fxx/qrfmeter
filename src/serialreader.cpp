#include "serialreader.h"

#include <iomanip>
#include <iostream>
#include <sstream>

SerialReader::SerialReader(QString const & serialPortName, float attenuation) :
_serialPortName{serialPortName},
_attenuation{attenuation},
_attenuationChanged{true},
_run{false} {
}

SerialReader::~SerialReader() {
    if (_run) {
        stop();
        wait();
    }
}

void SerialReader::stop() {
    _run = false;
}

void SerialReader::setAttenuation(float attenuation) {
    _attenuation = attenuation;
    _attenuationChanged = true;
}

void SerialReader::run() {
    if (_serialPortName.compare("") == 0) {
        quit();
        return;
    }

    QSerialPort serialPort;
    serialPort.setPortName(_serialPortName);
    serialPort.setBaudRate(QSerialPort::Baud115200);

    if (!serialPort.open(QIODevice::ReadWrite)) {
        std::cout << QObject::tr("Failed to open port, error: %1").arg(serialPort.errorString()).toStdString() << std::endl;
        quit();
        return;
    }

    // Get version
    if (serialPort.write(QByteArray{"rid#"}) != 4) {
        std::cout << "Didn't send get version" << std::endl;
    }

    // Force reset attenuation on each run
    _attenuationChanged = true;

    _run = true;

    while (_run) {
      if (_attenuationChanged) {
        _sendAttenuation(serialPort);
      }
      _readFrame(serialPort);
    }

    serialPort.close();

    quit();
}

void SerialReader::_sendAttenuation(QSerialPort & serialPort) {
  std::ostringstream os;

  os << "$" << std::fixed << std::setfill('0') << std::setw(4) << 100 \
    << std::showpos << std::internal << std::setw(5) << std::setprecision(1) \
    << -_attenuation << "#";

  if (serialPort.write(QByteArray::fromRawData(os.str().c_str(), os.str().size())) != 11) {
      std::cout << "Didn't send get version" << std::endl;
  }
  _attenuationChanged = false;
}

void SerialReader::_readFrame(QSerialPort & serialPort) {
    if (serialPort.waitForReadyRead(1000)) {
        QByteArray readData = serialPort.read(1);

        int readLength{1};

        if (readData[0] == '$') {
            readLength = 22;
        }
        else if (readData.at(0) == 'R') {
            readLength = 11;
        }
        else {
            std::cout << "unknown packet start: " << std::string(readData.constData(), readData.length()) << std::endl;
            return;
        }

        while (readData.size() < readLength && serialPort.waitForReadyRead(10)) {
            readData.append(serialPort.readAll());
        }

        if (serialPort.error() == QSerialPort::ReadError) {
            std::cout << QObject::tr("Failed to read from port %1, error: %2")
                              .arg(_serialPortName).arg(serialPort.errorString()).toStdString() << std::endl;
            return;
        } else if (serialPort.error() == QSerialPort::TimeoutError && readData.isEmpty()) {
            std::cout << QObject::tr("No data was currently available"
                                          " for reading from port %1")
                              .arg(_serialPortName).toStdString() << std::endl;
            return;
        }

        if (readData.size() == 22) {
            std::string v;

            std::copy(
                std::begin(readData) + 1,
                std::begin(readData) + 6,
                std::back_inserter(v)
            );

            std::cout << std::stof(v) << std::endl;
            emit newValueReady(std::stof(v));
        } else if (readData.size() == 11) {
            std::cout << "Version: " << std::string(readData.constData(), readData.length()) << std::endl;
            emit newDeviceName(QString::fromLatin1(readData.data()));
        }
    }
}
