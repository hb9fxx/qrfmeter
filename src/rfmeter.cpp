#include "rfmeter.h"

#include <algorithm>

RFMeter::RFMeter() :
QObject(nullptr),
_attenuator{0},
_rangeMin{-70.},
_rangeLow{-55.},
_rangeHigh{-10.},
_rangeMax{10.} {
    _value = -7.3;
}

QString RFMeter::deviceName() {
    return _deviceName;
}

QStringList RFMeter::deviceList() {
    QStringList list;

    std::transform(
        std::begin(_serialPorts),
        std::end(_serialPorts),
        std::back_inserter(list),
        [](auto const & e) { return e.portName(); }
    );

    std::sort(std::begin(list), std::end(list));

    return list;
}

void RFMeter::listSerialPorts() {
    _serialPorts.clear();

    auto serialPorts = QSerialPortInfo::availablePorts();

    for (auto const & port: serialPorts) {
        std::cout << port.description().toStdString() << " " << port.portName().toStdString() << " " << port.systemLocation().toStdString() << std::endl;
        _serialPorts.append(port);
    }

    emit deviceListChanged();
}

void RFMeter::setDeviceName(QString const & deviceName) {
    _deviceName = deviceName;
    emit deviceNameChanged();
}

void RFMeter::setDevice(QString const & device) {
    auto d = std::find_if(
        std::begin(_serialPorts),
        std::end(_serialPorts),
        [&device](auto const & e){ return e.portName() == device; }
    );

    if (d == std::end(_serialPorts)) {
        std::cout << "device not found" << std::endl;
        return;
     }

    std::cout << "device set to " << device.toStdString() << std::endl;


    _reader = std::make_unique<SerialReader>(d->systemLocation(), _attenuator);

    connect(_reader.get(), &SerialReader::newDeviceName, this, &RFMeter::setDeviceName);
    connect(_reader.get(), &SerialReader::newValueReady, this, &RFMeter::newValue);
    connect(this, &RFMeter::attenuatorChanged, _reader.get(), &SerialReader::setAttenuation);

    _reader->start();
}

void RFMeter::setAttenuator(float attenuator) {
  _attenuator = attenuator;
  emit rangesChanged();
  emit attenuatorChanged(_attenuator);
  std::cout << "Attenuator set to " << _attenuator << std::endl;
}

void RFMeter::newValue(float value) {
  _value = value;
  emit valueChanged();
}
