#pragma once

#include "serialreader.h"

#include <QObject>
#include <QSerialPortInfo>
#include <QString>
#include <QStringList>

#include <iostream>
#include <memory>

class RFMeter : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString deviceName READ deviceName NOTIFY deviceNameChanged)
    Q_PROPERTY(QStringList deviceList READ deviceList NOTIFY deviceListChanged)

    Q_PROPERTY(float attenuator READ attenuator WRITE setAttenuator NOTIFY rangesChanged)
    Q_PROPERTY(float rangeMin READ rangeMin NOTIFY rangesChanged)
    Q_PROPERTY(float rangeLow READ rangeLow NOTIFY rangesChanged)
    Q_PROPERTY(float rangeHigh READ rangeHigh NOTIFY rangesChanged)
    Q_PROPERTY(float rangeMax READ rangeMax NOTIFY rangesChanged)

    Q_PROPERTY(float value READ value NOTIFY valueChanged)

public:
    RFMeter();

    QString deviceName();
    QStringList deviceList();

    float attenuator() { return _attenuator; };
    float rangeMin() { return _rangeMin - _attenuator; };
    float rangeLow() { return _rangeLow - _attenuator; };
    float rangeHigh() { return _rangeHigh - _attenuator; };
    float rangeMax() { return _rangeMax - attenuator(); };

    float value() { return _value; };

signals:
    void deviceNameChanged();
    void deviceListChanged();

    void rangesChanged();
    void attenuatorChanged(float attenuator);

    void valueChanged();

public slots:
    void listSerialPorts();

    void setDeviceName(QString const & deviceName);
    void setDevice(QString const & device);

    void setAttenuator(float attenuator);

    void newValue(float value);

private:
    QString _deviceName;
    QList<QSerialPortInfo> _serialPorts;

    float _attenuator;
    const float _rangeMin;
    const float _rangeLow;
    const float _rangeHigh;
    const float _rangeMax;

    float _value;

    std::unique_ptr<SerialReader> _reader;
};
