#pragma once

#include <QObject>
#include <QSerialPort>
#include <QThread>

#include <atomic>

class SerialReader : public QThread {
    Q_OBJECT

public:
    explicit SerialReader(
      QString const & serialPortName = "",
      float attenuation = 0.0
    );
    ~SerialReader();

    void stop();

signals:
    void newDeviceName(QString const & deviceName);
    void newValueReady(float value);

public slots:
    void setAttenuation(float attenuation);

private:
    void run() override;
    void _sendAttenuation(QSerialPort & serialPort);
    void _readFrame(QSerialPort & serialPort);

    QString _serialPortName;
    float _attenuation;
    std::atomic_bool _attenuationChanged;
    std::atomic_bool _run;
};
